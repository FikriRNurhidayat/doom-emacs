(package! rainbow-mode)

(package! solaire-mode :disable t)

(package! org-modern)

(package! org-appear)

(package! org-present)

(unpin! org-roam)
(package! org-roam-ui)

(package! xterm-color)

(package! ripgrep)

(package! protobuf-mode)
